﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;


public enum Message
{
    NewTerrain, SaveTerrain, SetBrush, GetBrush, GetFalloffs,
    GetSaves,
    ResetTerrainZoom,
    ResetTerrainPosition,
    LoadTerrain,
    ToggleTable,
}

public class StateManager : MonoBehaviour
{
    #region Fields

    private static bool applicationIsQuitting = false;
    private static object m_Lock = new object();

    public enum ProgramState
    {
        Editing, TableAdjustment, Menu
    }

    public enum ModelingState
    {
        None, Panning, VertexManipulating, Placing
    }


    private static StateManager _instance;

    private ProgramState currentProgramState = ProgramState.Editing;
    // private ProgramState currentState = ProgramState.TableAdjustment;
    private ProgramState lastProgramState = ProgramState.Editing;

    // private ModelingState currentModelingState = ModelingState.VertexManipulating;
    private ModelingState currentModelingState = ModelingState.Panning;


    private List<IMessageReceiver> receiverList;

    #endregion

    #region Properties

    public static StateManager Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                return null;
            }
            lock (m_Lock)
            {
                if (_instance == null)
                {
                    _instance = new GameObject("StateManager").AddComponent<StateManager>();
                }
                return _instance;
            }
        }
    }

    public ProgramState CurrentProgramState
    {
        set
        {
            currentProgramState = value;
        }

        get
        {
            return currentProgramState;
        }
    }

    public ProgramState LastProgramState
    {
        set
        {
            lastProgramState = value;
        }

        get
        {
            return lastProgramState;
        }
    }

    public ModelingState CurrentModelingState
    {
        set
        {
            currentModelingState = value;
        }

        get
        {
            return currentModelingState;
        }
    }



    #endregion

    #region Functions

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        receiverList = new List<IMessageReceiver>();

        DontDestroyOnLoad(gameObject);

    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
        Debug.Log("Gets destroyed");
        applicationIsQuitting = true;
    }

    public object[] Notify(Message message, params object[] data)
    {
        object[] back = null;
        foreach (IMessageReceiver rec in receiverList.ToList())
        {
            if (rec != null)
            {
                back = rec.ReceiveMessage(message, data);
                if (!(back is null))
                {
                    return back;
                }
            }
        }
        Debug.LogWarning("No objects returned or no receiver found for Message \"" + message.ToString() + "\". If returned objects where expected, check the targeted script.");
        return back;
    }

    public void AddReceiver(IMessageReceiver receiver)
    {
        receiverList.Add(receiver);
    }

    public bool RemoveReceiver(IMessageReceiver receiver)
    {
        return receiverList.Remove(receiver);
    }

    public void ExitApplication()
    {
        Debug.Log("Exit now?");
        Application.Quit();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif

    }


    /// <summary>
    /// Method to run coroutines from everywhere
    /// </summary>
    /// <param name="_coroutine"></param>
    /// <returns>Returns a new coroutine</returns>
    public Coroutine RunCoroutine(IEnumerator _coroutine)
    {
        Debug.Log("Starting new coroutine. " + _coroutine.ToString());
        return this.StartTrackedCoroutine(_coroutine);
    }

    #endregion
}

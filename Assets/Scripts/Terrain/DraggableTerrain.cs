﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;

public class DraggableTerrain : BasicGrabbable
{
    public override void OnColliderEventDragStart(HTC.UnityPlugin.ColliderEvent.ColliderButtonEventData eventData)
    {
        if (StateManager.Instance.CurrentModelingState == StateManager.ModelingState.Panning)
        {
            base.OnColliderEventDragStart(eventData);
        }
    }

    public override void OnColliderEventDragUpdate(HTC.UnityPlugin.ColliderEvent.ColliderButtonEventData eventData)
    {
        base.OnColliderEventDragUpdate(eventData);
        transform.rotation = Quaternion.identity;
    }
}

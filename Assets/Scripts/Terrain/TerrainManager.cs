using HTC.UnityPlugin.Vive;
using UnityEngine;
using Utils;

public class TerrainManager : MonoBehaviour, IMessageReceiver
{
    #region Fields

    [SerializeField] private GameObject customTerrainParent;
    [SerializeField] private float maxScale = 1000f;
    [SerializeField] private float minScale = .0001f;
    [SerializeField] private GameObject terrainMask;
    [SerializeField] private Falloff[] falloffs;


    private Falloff currentFalloff;
    private int brushSize = 20;
    private float brushHardness = 1;

    private CustomTerrain currentTerrain;

    private Vector3 oldPos = default;
    private float oldZoomDistance;

    #endregion

    #region Functions

    void Start()
    {
        StateManager.Instance.AddReceiver(this);

        currentFalloff = falloffs[0]; // Legt den standard Falloff fest
        // CreateNewTerrain("default", 128, 128);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (StateManager.Instance.CurrentProgramState == StateManager.ProgramState.Editing)
        {

            if (ViveInput.GetPressDownEx(HandRole.RightHand, ControllerButton.Grip)
            && ViveInput.GetPressDownEx(HandRole.LeftHand, ControllerButton.Grip))
            {
                oldZoomDistance = Vector3.Distance(VivePose.GetPose(HandRole.RightHand).pos, VivePose.GetPose(HandRole.LeftHand).pos);
            }
            if (ViveInput.GetPressEx(HandRole.RightHand, ControllerButton.Grip)
            && ViveInput.GetPressEx(HandRole.LeftHand, ControllerButton.Grip))
            {
                float newZoomDistance = Vector3.Distance(VivePose.GetPose(HandRole.RightHand).pos, VivePose.GetPose(HandRole.LeftHand).pos);
                float change = newZoomDistance / oldZoomDistance;

                if (Mathf.Abs(change) > 0)
                {
                    TerrainZoom(change);
                }

                oldZoomDistance = newZoomDistance;
            }

            if (StateManager.Instance.CurrentModelingState == StateManager.ModelingState.VertexManipulating)
            {
                if (ViveInput.GetPressDownEx(HandRole.RightHand, ControllerButton.Trigger))
                {
                    Vector3 oldPos = VivePose.GetPose(HandRole.RightHand).pos;
                }
                else if (ViveInput.GetPressEx(HandRole.RightHand, ControllerButton.Trigger))
                {
                    ManipulateTerrain();
                }
            }
        }
    }

    private void ManipulateTerrain()
    {
        Vector3 newPos = VivePose.GetPose(HandRole.RightHand).pos;
        Vector3 translation = new Vector3(0, newPos.y - oldPos.y, 0);

        currentTerrain.DisplaceVertices(oldPos, translation, brushSize, currentFalloff.Type, brushHardness);


        oldPos = newPos;
    }

    private void CreateNewTerrain(string name, int xRes, int zRes)
    {
        if (currentTerrain)
        {
            Destroy(currentTerrain.gameObject);
        }

        currentTerrain = ObjectUtils.Instantiate<CustomTerrain>(customTerrainParent.transform, name);
        currentTerrain.Resolution = new Vector2(xRes, zRes);
        currentTerrain.Name = name;

        currentTerrain.CreateTerrain();
    }

    public bool Save()
    {
        return SaveTerrainManager.SaveTerrainData(currentTerrain.ToSaveTerrainData());
    }

    public bool Load(string terrainName)
    {
        SaveTerrainData data = SaveTerrainManager.LoadTerrainData(terrainName);

        if (data is null)
        {
            return false;
        }

        Destroy(currentTerrain.gameObject);

        currentTerrain = ObjectUtils.Instantiate<CustomTerrain>(customTerrainParent.transform, data.name);
        currentTerrain.LoadMesh(data);

        return true;
    }

    private string[] GetSavedTerrains()
    {
        return SaveTerrainManager.GetSavedTerrains();
    }

    private void SetFalloff(string falloffID)
    {
        foreach (Falloff fo in falloffs)
        {
            if (fo.FalloffID.Equals(falloffID))
            {
                currentFalloff = fo;
                return;
            }
        }
    }

    private void TerrainZoom(float zoomChange, bool reset = false)
    {
        if (reset)
        {
            customTerrainParent.transform.localScale = Vector3.one;
            return;
        }

        customTerrainParent.transform.localScale += (Vector3.one - new Vector3(zoomChange, zoomChange, zoomChange));

        customTerrainParent.transform.localScale = new Vector3(
            Mathf.Clamp(customTerrainParent.transform.localScale.x, minScale, maxScale),
            Mathf.Clamp(customTerrainParent.transform.localScale.y, minScale, maxScale),
            Mathf.Clamp(customTerrainParent.transform.localScale.z, minScale, maxScale));
    }


    public void UpdateParentYPosition(float yPos)
    {
        Vector3 newHeight = customTerrainParent.transform.localPosition;
        newHeight.y = yPos;
        customTerrainParent.transform.localPosition = newHeight;
    }

    public object[] ReceiveMessage(Message message, object[] data)
    {
        switch (message)
        {
            case Message.NewTerrain:
                CreateNewTerrain(System.Convert.ToString(data[0]),
                System.Convert.ToInt32(data[1]),
                System.Convert.ToInt32(data[2]));
                break;
            case Message.SaveTerrain:
                Save();
                break;
            case Message.LoadTerrain:
                Load(System.Convert.ToString(data[0]));
                break;
            case Message.GetSaves:
                return GetSavedTerrains();
            case Message.SetBrush:
                SetFalloff(System.Convert.ToString(data[0]));
                brushSize = System.Convert.ToInt32(data[1]);
                brushHardness = System.Convert.ToSingle(data[2]);
                break;
            case Message.GetBrush:
                return new object[] { currentFalloff.FalloffID, brushSize, brushHardness };
            case Message.GetFalloffs:
                return falloffs;
            case Message.ResetTerrainZoom:
                TerrainZoom(0, true);
                break;
            default:
                break;
        }

        return null;
    }

    #endregion
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using mattatz.MeshSmoothingSystem;
using UnityEngine;
using Utils;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class CustomTerrain : MonoBehaviour, IMessageReceiver
{

    #region Field

    [SerializeField] private Material terrainMaterial;
    [SerializeField] private int xResolution = 128;
    [SerializeField] private int zResolution = 128;


    private string terrainName;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private MeshCollider meshCollider;


    private MeshData terrainData;
    private List<GameObject> placedProps;

    #endregion

    #region Properties

    public string Name
    {
        get
        {
            return terrainName;
        }

        set
        {
            terrainName = value;
        }
    }

    public Vector2 Resolution
    {
        get
        {
            return new Vector2(xResolution, zResolution);
        }
        set
        {
            xResolution = (int)value.x;
            zResolution = (int)value.y;
        }
    }

    #endregion

    #region Functions

    void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshCollider = GetComponent<MeshCollider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StateManager.Instance.AddReceiver(this);
    }

    void OnDestroy()
    {
        if (StateManager.Instance != null)
        {
            StateManager.Instance.RemoveReceiver(this);
        }
    }

    public void CreateTerrain()
    {
        float xSize = 1, zSize = 1;
        if (xResolution > zResolution)
        {
            xSize = xResolution / (float)zResolution;
        }
        if (xResolution < zResolution)
        {
            zSize = zResolution / (float)xResolution;
        }

        terrainData = MeshGenerator.GenerateMeshTerrain(xResolution, zResolution, 1, 1);
        meshFilter.mesh = terrainData.generatedMesh;
        meshCollider.sharedMesh = terrainData.generatedMesh;

        meshRenderer.material = terrainMaterial;
    }

    public SaveTerrainData ToSaveTerrainData()
    {
        SaveTerrainData saveData = new SaveTerrainData();
        saveData.name = terrainName;
        saveData.previewImage = null;

        saveData.vertices = terrainData.m_vertices;
        saveData.uvs = terrainData.m_uvs;
        saveData.triangles = terrainData.m_triangles;

        return saveData;
    }

    public void LoadMesh(SaveTerrainData data)
    {
        terrainData = new MeshData(data.name, data.vertices, data.uvs, data.triangles);

        meshFilter.mesh = terrainData.generatedMesh;
        meshCollider.sharedMesh = terrainData.generatedMesh;

        meshRenderer.material = terrainMaterial;
    }

    public void DisplaceVertices(Vector3 currentPoint, Vector3 translation, int range, Falloff.FalloffType type, float hardness)
    {
        float convertedRange = MathUtils.Map(1, 100, 0.01f, 1f, range);
        Vector3 transformedPoint = transform.parent.transform.InverseTransformPoint(currentPoint);

        // Sucht die Indices aller beeinflussten Vertices
        int[] verts = terrainData.m_vertices
                                    .Select((s, i) => new { i, s })
                                    .Where(v => (Vector3.Distance(v.s, transformedPoint) <= convertedRange))
                                    .Select(v => v.i).ToArray();

        foreach (int item in verts)
        {
            Vector3 transFO = transform.parent.transform.InverseTransformVector(translation);
            float deltaDistance = 1 - Vector3.Distance(terrainData.m_vertices[item], transformedPoint);
            int exponent = 0;
            switch (type)
            {
                case Falloff.FalloffType.Constant:
                    break;
                case Falloff.FalloffType.Linear:
                    exponent = 1;
                    break;
                case Falloff.FalloffType.Square:
                    exponent = 2;
                    break;
            }

            transFO *= Mathf.Lerp(1, Mathf.Pow(deltaDistance, exponent), hardness);

            terrainData.m_vertices[item].Set(terrainData.m_vertices[item].x + transFO.x, terrainData.m_vertices[item].y + transFO.y, terrainData.m_vertices[item].z + transFO.z);
        }

        terrainData.UpdateMesh();
        terrainData.SmoothMesh();
    }

    public void PlaceProp(GameObject prop)
    {
        placedProps.Add(prop);
    }

    public void DeleteProp(GameObject prop)
    {
        placedProps.Remove(prop);
    }

    public object[] ReceiveMessage(Message message, object[] data)
    {
        switch (message)
        {
            case Message.ResetTerrainPosition:
                transform.localPosition = Vector3.zero;
                break;
            default:
                break;
        }

        return null;
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

[Serializable]
public class Falloff
{
    [Serializable]
    public enum FalloffType
    {
        Constant, Linear, Square
    }

    [SerializeField] private string _name;
    [SerializeField] private string falloffID;
    [SerializeField] private FalloffType type;
    [SerializeField] private Texture image;

    public Texture Image
    {
        get
        {
            return image;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }
    }

    public string FalloffID
    {
        get
        {
            return falloffID;
        }
    }

    public FalloffType Type
    {
        get
        {
            return type;
        }
    }

    private Falloff()
    {

    }

}

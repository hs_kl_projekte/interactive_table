// Source: https://www.youtube.com/watch?v=4RpVBYW1r5M

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using mattatz.MeshSmoothingSystem;
using System;
using System.Runtime.Serialization;

public static class MeshGenerator
{
    public static MeshData GenerateMeshTerrain(int meshResX, int meshResZ, float meshWidthX, float meshWidthZ)
    {
        float widthX = meshWidthX;
        float widthZ = meshWidthZ;

        int resX = meshResX;
        int resZ = meshResZ;

        float topLeftX = (resX - 1) * -0.5f * (widthX / resX);
        float topLeftZ = (resZ - 1) * 0.5f * (widthZ / resZ);


        MeshData meshData = new MeshData(resX, resZ);

        int vertIndex = 0;

        for (int z = 0; z < resZ; z++)
        {
            for (int x = 0; x < resX; x++)
            {
                meshData.m_vertices[vertIndex] = new Vector3(
                    topLeftX + x * (widthX / resX),
                    0,
                    topLeftZ - z * (widthZ / resZ));

                meshData.m_uvs[vertIndex] = new Vector2(x / (float)resX, z / (float)resZ);

                if (x < resX - 1 && z < resZ - 1)
                {
                    meshData.AddTriangle(vertIndex, vertIndex + resX + 1, vertIndex + resX);
                    meshData.AddTriangle(vertIndex + resX + 1, vertIndex, vertIndex + 1);
                }

                vertIndex++;
            }

        }

        meshData.CreateMesh();

        return meshData;
    }
}

public class MeshData
{
    public Vector3[] m_vertices;
    public Vector2[] m_uvs;
    public int[] m_triangles;
    public Mesh generatedMesh;

    int triangleIndex;

    public MeshData(int meshWidth, int meshHeight)
    {
        m_vertices = new Vector3[meshWidth * meshHeight];
        m_uvs = new Vector2[meshWidth * meshHeight];
        m_triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];
        Debug.Log("Vert: " + m_vertices.Length + " Tris: " + m_triangles.Length);
    }

    public MeshData(string name, Vector3[] vertices, Vector2[] uvs, int[] triangles)
    {
        m_vertices = vertices;
        m_uvs = uvs;
        m_triangles = triangles;

        generatedMesh = new Mesh();
        generatedMesh.name = name;
        generatedMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        generatedMesh.MarkDynamic();


        generatedMesh.vertices = m_vertices;
        generatedMesh.triangles = m_triangles;
        generatedMesh.uv = m_uvs;

        generatedMesh.RecalculateNormals();
    }

    public void AddTriangle(int a, int b, int c)
    {
        m_triangles[triangleIndex + 0] = a;
        m_triangles[triangleIndex + 1] = b;
        m_triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public void CreateMesh()
    {
        generatedMesh = new Mesh();
        generatedMesh.name = "GeneratedTerrainMesh";
        generatedMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        generatedMesh.MarkDynamic();

        generatedMesh.vertices = m_vertices;
        generatedMesh.triangles = m_triangles;
        generatedMesh.uv = m_uvs;

        generatedMesh.RecalculateNormals();

    }

    public void UpdateMesh()
    {

        generatedMesh.Clear();

        generatedMesh.vertices = m_vertices;
        generatedMesh.uv = m_uvs;
        generatedMesh.triangles = m_triangles;

        generatedMesh.RecalculateNormals();
    }

    public void SmoothMesh()
    {
        generatedMesh = MeshSmoothing.LaplacianFilter(generatedMesh, 3);
    }
}
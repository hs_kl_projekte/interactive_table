﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable] 
public class SaveTerrainData
{
    public string name;
    public RawImage previewImage;
    public Vector3[] vertices;
    public Vector2[] uvs;
    public int[] triangles;
    public Material material;
}

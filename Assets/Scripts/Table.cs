﻿using UnityEngine;

public class Table : MonoBehaviour
{
    #region Fields
    [SerializeField] private GameObject tablePlate;
    [SerializeField] private GameObject[] tableLegs;
    [SerializeField] private Material tablePlateMaterial;
    [SerializeField] private Material tableLegsMaterial;

    private float tablePlateWidth_x = 2f;
    private float tablePlateHeight = 0.1f;
    private float tablePlateWidth_z = 2f;
    private float tableHeight = 1f;


    private MeshFilter plateMeshFilter;
    private MeshFilter[] legsMeshFilter;
    private MeshRenderer plateMeshRenderer;
    private MeshRenderer[] legsMeshRenderer;
    private bool changed = false;
    #endregion

    #region Properties


    #endregion


    #region Functions

    void Start()
    {
        plateMeshFilter = tablePlate.GetComponent<MeshFilter>();
        plateMeshRenderer = tablePlate.GetComponent<MeshRenderer>();
        plateMeshRenderer.material = tablePlateMaterial;

        legsMeshFilter = new MeshFilter[tableLegs.Length];
        legsMeshRenderer = new MeshRenderer[tableLegs.Length];

        for (int i = 0; i < tableLegs.Length; i++)
        {
            legsMeshFilter[i] = tableLegs[i].GetComponent<MeshFilter>();
            legsMeshRenderer[i] = tableLegs[i].GetComponent<MeshRenderer>();
            legsMeshRenderer[i].material = tableLegsMaterial;
        }

        UpdateTablePlate();
        UpdateTableLegs();
    }

    private void UpdateTablePlate()
    {
        Vector3[] c = new Vector3[8];

        c[0] = new Vector3(-tablePlateWidth_x * .5f, -tablePlateHeight, tablePlateWidth_z * .5f);
        c[1] = new Vector3(tablePlateWidth_x * .5f, -tablePlateHeight, tablePlateWidth_z * .5f);
        c[2] = new Vector3(tablePlateWidth_x * .5f, -tablePlateHeight, -tablePlateWidth_z * .5f);
        c[3] = new Vector3(-tablePlateWidth_x * .5f, -tablePlateHeight, -tablePlateWidth_z * .5f);

        c[4] = new Vector3(-tablePlateWidth_x * .5f, 0, tablePlateWidth_z * .5f);
        c[5] = new Vector3(tablePlateWidth_x * .5f, 0, tablePlateWidth_z * .5f);
        c[6] = new Vector3(tablePlateWidth_x * .5f, 0, -tablePlateWidth_z * .5f);
        c[7] = new Vector3(-tablePlateWidth_x * .5f, 0, -tablePlateWidth_z * .5f);

        Vector3[] vertices =
        {
            c[0], c[1], c[2], c[3], // Bottom
	        c[7], c[4], c[0], c[3], // Left
	        c[4], c[5], c[1], c[0], // Front
	        c[6], c[7], c[3], c[2], // Back
	        c[5], c[6], c[2], c[1], // Right
	        c[7], c[6], c[5], c[4]  // Top
        };

        int[] tris =
        {
            3, 1, 0,        3, 2, 1,        // Bottom	
	        7, 5, 4,        7, 6, 5,        // Left
	        11, 9, 8,       11, 10, 9,      // Front
	        15, 13, 12,     15, 14, 13,     // Back
	        19, 17, 16,     19, 18, 17,	    // Right
	        23, 21, 20,     23, 22, 21	    // Top
        };

        Mesh mesh = plateMeshFilter.mesh;
        mesh.Clear();
        mesh.name = "tablePlateMesh";

        mesh.vertices = vertices;
        mesh.triangles = tris;

        mesh.RecalculateNormals();

        Vector3 locPos = tablePlate.transform.localPosition;
        tablePlate.transform.localPosition = new Vector3(locPos.x, tableHeight, locPos.z);
    }

    private void UpdateTableLegs()
    {
        for (int i = 0; i < tableLegs.Length; i++)
        {
            Vector3[] c = new Vector3[8];

            c[0] = new Vector3(-.1f, 0, .1f);
            c[1] = new Vector3(.1f, 0, .1f);
            c[2] = new Vector3(.1f, 0, -.1f);
            c[3] = new Vector3(-.1f, 0, -.1f);

            c[4] = new Vector3(-.1f, tableHeight - tablePlateHeight * 0.5f, .1f);
            c[5] = new Vector3(.1f, tableHeight - tablePlateHeight * 0.5f, .1f);
            c[6] = new Vector3(.1f, tableHeight - tablePlateHeight * 0.5f, -.1f);
            c[7] = new Vector3(-.1f, tableHeight - tablePlateHeight * 0.5f, -.1f);

            Vector3[] vertices =
            {
                c[0], c[1], c[2], c[3], // Bottom
                c[7], c[4], c[0], c[3], // Left
                c[4], c[5], c[1], c[0], // Front
                c[6], c[7], c[3], c[2], // Back
                c[5], c[6], c[2], c[1], // Right
                c[7], c[6], c[5], c[4]  // Top
            };

            int[] tris =
            {
                3, 1, 0,        3, 2, 1,        // Bottom	
                7, 5, 4,        7, 6, 5,        // Left
                11, 9, 8,       11, 10, 9,      // Front
                15, 13, 12,     15, 14, 13,     // Back
                19, 17, 16,     19, 18, 17,	    // Right
                23, 21, 20,     23, 22, 21	    // Top
            };

            Mesh mesh = legsMeshFilter[i].mesh;
            mesh.Clear();
            mesh.name = "tableLegMesh_" + i;

            mesh.vertices = vertices;
            mesh.triangles = tris;

            mesh.RecalculateNormals();

        }

        tableLegs[0].transform.localPosition = new Vector3(-tablePlateWidth_x * .5f + .15f, 0, tablePlateWidth_z * .5f - .15f);
        tableLegs[1].transform.localPosition = new Vector3(tablePlateWidth_x * .5f - .15f, 0, tablePlateWidth_z * .5f - .15f);
        tableLegs[2].transform.localPosition = new Vector3(tablePlateWidth_x * .5f - .15f, 0, -tablePlateWidth_z * .5f + .15f);
        tableLegs[3].transform.localPosition = new Vector3(-tablePlateWidth_x * .5f + .15f, 0, -tablePlateWidth_z * .5f + .15f);
    }

    public void UpdateTable(float newWidthX, float newWidthZ, float newTableHeight)
    {
        tablePlateWidth_x = newWidthX;
        tablePlateWidth_z = newWidthZ;

        tableHeight = newTableHeight;

        UpdateTablePlate();
        UpdateTableLegs();
    }

    #endregion
}

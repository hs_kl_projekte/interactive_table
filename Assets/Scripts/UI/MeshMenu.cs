﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeshMenu : MonoBehaviour
{

    [SerializeField] private DataButton createButton;
    [SerializeField] private DataButton saveButton;
    [SerializeField] private DataButton loadButton;

    [SerializeField] private GameObject selectMenu;
    [SerializeField] private GameObject createMesh;
    [SerializeField] private GameObject loadMesh;


    // Start is called before the first frame update
    void Start()
    {
        createButton.GetComponent<Button>().onClick.AddListener(CreateOnClick);
        saveButton.GetComponent<Button>().onClick.AddListener(SaveOnClick);
        loadButton.GetComponent<Button>().onClick.AddListener(LoadOnClick);

        createMesh.SetActive(false);
        loadMesh.SetActive(false);
    }

    void CreateOnClick()
    {
        selectMenu.SetActive(false);
        createMesh.SetActive(true);
        //Add Listener für Acceptcreate button
        //acceptCreate.GetComponent<Button>().onClick.AddListener(acceptCreate);
    }

    void SaveOnClick()
    {
        StateManager.Instance.Notify(Message.SaveTerrain);
    }

    void LoadOnClick()
    {
        loadMesh.SetActive(true);
        selectMenu.SetActive(false);
    }

    void onAcceptCreate()
    {
        createMesh.SetActive(false);
        StateManager.Instance.Notify(Message.NewTerrain);
    }

}

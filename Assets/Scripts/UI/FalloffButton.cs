﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalloffButton : MonoBehaviour
{

    private string falloffID;

    public string FalloffID
    {
        get
        {
            return falloffID;
        }

        set
        {
            falloffID = value;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using UnityEngine.UI;
using Utils;


public class CreateMesh : MonoBehaviour
{

    [SerializeField] private Button fileNameButton;
    private string defaultFileText = "Dateinamen eingeben";

    [SerializeField] private Button xResButton;
    private string defaultXResText = "X-Res";

    [SerializeField] private Button yResButton;
    private string defaultYResText = "Y-Res";

    [SerializeField] private Button acceptCreateButton;
    [SerializeField] private Button backButton;

    [SerializeField] private VRKeyboard keyboard;

    [SerializeField] private GameObject meshMenu;

    // Start is called before the first frame update
    void Start()
    {
        fileNameButton.GetComponent<Button>().onClick.AddListener(CreateFileNameClick);
        xResButton.GetComponent<Button>().onClick.AddListener(XResClick);
        yResButton.GetComponent<Button>().onClick.AddListener(YResClick);
        acceptCreateButton.GetComponent<Button>().onClick.AddListener(AcceptCreateClick);
        backButton.GetComponent<Button>().onClick.AddListener(BackClick);


        keyboard.gameObject.SetActive(false);
    }

    void CreateFileNameClick()
    {
        keyboard.gameObject.SetActive(true);
        keyboard.SetTextfield = fileNameButton.GetComponentInChildren<Text>();
    }

    void XResClick()
    {
        keyboard.gameObject.SetActive(true);
        keyboard.SetTextfield = xResButton.GetComponentInChildren<Text>();
    }

    void YResClick()
    {
        keyboard.gameObject.SetActive(true);
        keyboard.SetTextfield = yResButton.GetComponentInChildren<Text>();
    }

    void AcceptCreateClick()
    {
        keyboard.gameObject.SetActive(false);
        StateManager.Instance.Notify(
            Message.NewTerrain, 
            fileNameButton.GetComponentInChildren<Text>().text, 
            xResButton.GetComponentInChildren<Text>().text, 
            yResButton.GetComponentInChildren<Text>().text);

        BackClick();
    }

    void BackClick()
    {
        fileNameButton.GetComponentInChildren<Text>().text = defaultFileText;
        xResButton.GetComponentInChildren<Text>().text = defaultXResText;
        yResButton.GetComponentInChildren<Text>().text = defaultYResText;

        meshMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.ColliderEvent;
using UnityEngine;

public class ResetTerrainPosition : MonoBehaviour, IColliderEventPressEnterHandler, IColliderEventPressExitHandler, IColliderEventPressUpHandler
{
    [SerializeField] private GameObject button;
    [SerializeField] private Vector3 buttonDisplacement;
    [SerializeField] private ColliderButtonEventData.InputButton m_activeButton = ColliderButtonEventData.InputButton.Trigger;

    private HashSet<ColliderButtonEventData> pressingEvents = new HashSet<ColliderButtonEventData>();

    private Vector3 basePos;

    // Start is called before the first frame update
    void Start()
    {
        basePos = button.transform.position;

        // UpdateButtonPress();
    }


    public void OnColliderEventPressEnter(ColliderButtonEventData eventData)
    {
        if (eventData.button == m_activeButton && pressingEvents.Add(eventData) && pressingEvents.Count == 1)
        {
            button.transform.localPosition += buttonDisplacement;

        }
    }

    public void OnColliderEventPressExit(ColliderButtonEventData eventData)
    {
        if (pressingEvents.Remove(eventData) && pressingEvents.Count == 0)
        {
            button.transform.localPosition -= buttonDisplacement;
        }
    }

    public void OnColliderEventPressUp(ColliderButtonEventData eventData)
    {
        if (pressingEvents.Contains(eventData) && pressingEvents.Count == 1)
        {
            StateManager.Instance.Notify(Message.ResetTerrainPosition);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class LoadSelect : MonoBehaviour
{

    #region Fields

    [SerializeField] private GameObject listViewport;
    [SerializeField] private GameObject selectMenu;
    [SerializeField] private Button backButton;

    private string[] saves;
    private List<LoadTerrainButton> loadButtons = new List<LoadTerrainButton>();

    #endregion


    public List<LoadTerrainButton> LoadButtons
    {
        get
        {
            return loadButtons;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(BackOnClick);

        // object[] savesData = StateManager.Instance.Notify(Message.GetSaves);
        // if (savesData is null)
        // {
        //     return;
        // }

        // saves = Array.ConvertAll(savesData, item => item as string);

        // foreach (string item in saves)
        // {
        //     LoadTerrainButton loadButton = ObjectUtils.Instantiate<LoadTerrainButton>(listViewport.transform, item);
        //     loadButton.transform.localScale = Vector3.one;
        //     loadButton.GetComponent<Button>().onClick.AddListener(() => LoadSave(item));
        //     loadButton.transform.GetComponentInChildren<Text>().text = item;
        //     loadButtons.Add(loadButton);
        // }

    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        LoadSavesAsButton();
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        foreach (LoadTerrainButton item in loadButtons)
        {
            if (item)
            {
                Destroy(item.gameObject);
            }
        }
    }

    void LoadSavesAsButton()
    {

        object[] savesData = StateManager.Instance.Notify(Message.GetSaves);
        if (savesData is null)
        {
            return;
        }

        saves = Array.ConvertAll(savesData, item => item as string);

        foreach (string item in saves)
        {
            LoadTerrainButton loadButton = ObjectUtils.Instantiate<LoadTerrainButton>(listViewport.transform, item);
            loadButton.transform.localScale = Vector3.one;
            loadButton.GetComponent<Button>().onClick.AddListener(() => LoadSave(item));
            loadButton.transform.GetComponentInChildren<Text>().text = item;
            loadButtons.Add(loadButton);
        }
    }

    void BackOnClick()
    {
        selectMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    void LoadSave(string name)
    {
        StateManager.Instance.Notify(Message.LoadTerrain, name);
        BackOnClick();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using UnityEngine.UI;

public class ModeMenu : MonoBehaviour
{

    #region Fields

    [SerializeField] private GameObject canvas;
    [SerializeField]
    private Button modeling;
    [SerializeField]
    private Button tableAdjustment;
    [SerializeField]
    private Button exit;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        modeling.onClick.AddListener(ClickModeling);
        tableAdjustment.onClick.AddListener(ClickTableAdjustment);
        exit.onClick.AddListener(ClickExit);
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        MarkActiveState();
    }

    void ClickModeling()
    {
        StateManager.Instance.LastProgramState = StateManager.Instance.CurrentProgramState;
        StateManager.Instance.CurrentProgramState = StateManager.ProgramState.Editing;
        gameObject.SetActive(false);
    }

    void ClickTableAdjustment()
    {
        StateManager.Instance.LastProgramState = StateManager.Instance.CurrentProgramState;
        StateManager.Instance.CurrentProgramState = StateManager.ProgramState.TableAdjustment;
        gameObject.SetActive(false);
    }

    void ClickExit()
    {
        StateManager.Instance.ExitApplication();
    }

    void MarkActiveState()
    {
        switch (StateManager.Instance.CurrentProgramState)
        {
            case StateManager.ProgramState.Editing:
                modeling.GetComponent<Image>().color = Color.yellow;
                tableAdjustment.GetComponent<Image>().color = Color.white;
                break;

            case StateManager.ProgramState.TableAdjustment:
                tableAdjustment.GetComponent<Image>().color = Color.yellow;
                modeling.GetComponent<Image>().color = Color.white;
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRKeyboard : MonoBehaviour
{
    private Text textField;
    private bool shiftActive = true;
    private bool intOnly = false;

    private List<Button> keys = new List<Button>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            keys.AddRange(transform.GetChild(i).GetComponentsInChildren<Button>());
        }

        foreach (Button item in keys)
        {
            item.onClick.AddListener(() => Keypress(item.gameObject.name));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Text SetTextfield
    {
        set
        {
            textField = value;
            textField.text = "";
            if (textField.transform.parent.name.Equals("FilenameButton"))
            {
                intOnly = false;
            }
            else
            {
                intOnly = true;
            }
        }
    }

    void Keypress(string key)
    {
        switch(key){

            case"Shift":
                shiftActive = !shiftActive;
                break;

            case "Space":
                textField.text = textField.text + " ";
                break;

            case "Enter":
                //keien Funktion
                break;

            case "Backspace":
                textField.text = textField.text.Substring(0, textField.text.Length - 1);
                break;

            //Reihe 1
            case "1":
                textField.text = textField.text + "1";
                shiftActive = false;
                break;
            case "2":
                textField.text = textField.text + "2";
                shiftActive = false;
                break;
            case "3":
                textField.text = textField.text + "3";
                shiftActive = false;
                break;
            case "4":
                textField.text = textField.text + "4";
                shiftActive = false;
                break;
            case "5":
                textField.text = textField.text + "5";
                shiftActive = false;
                break;
            case "6":
                textField.text = textField.text + "6";
                shiftActive = false;
                break;
            case "7":
                textField.text = textField.text + "7";
                shiftActive = false;
                break;
            case "8":
                textField.text = textField.text + "8";
                shiftActive = false;
                break;
            case "9":
                textField.text = textField.text + "9";
                shiftActive = false;
                break;
            case "0":
                textField.text = textField.text + "0";
                shiftActive = false;
                break;

            //Reihe 2
            case "Q":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Q";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "q";
                }
                }

                break;
            case "W":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "W";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "w";
                }
                }

                break;
            case "E":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "E";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "e";
                }
                }

                break;
            case "R":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "R";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "r";
                }
                }

                break;
            case "T":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "T";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "t";
                }
                }

                break;
            case "Z":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Z";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "z";
                }
                }

                break;
            case "U":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "U";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "u";
                }
                }

                break;
            case "I":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "I";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "i";
                }
                }

                break;
            case "O":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "O";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "o";
                }
                }

                break;
            case "P":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "P";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "p";
                }
                }

                break;
            case "Ü":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Ü";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "ü";
                }
                }

                break;



            //Reihe 3
            case "A":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "A";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "a";
                }
                }

                break;
            case "S":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "S";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "s";
                }
                }

                break;
            case "D":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "D";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "d";
                }
                }

                break;
            case "F":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "F";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "f";
                }
                }

                break;
            case "G":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "G";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "g";
                }
                }

                break;
            case "H":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "H";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "h";
                }
                }

                break;
            case "J":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "J";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "j";
                }
                }

                break;
            case "K":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "K";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "k";
                }
                }

                break;
            case "L":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "L";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "l";
                }
                }

                break;
            case "Ö":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Ö";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "ö";
                }
                }

                break;
            case "Ä":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Ä";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "ä";
                }
                }

                break;


            //Reihe 4
            case "Y":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "Y";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "y";
                }
                }

                break;
            case "X":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "X";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "x";
                }
                }

                break;
            case "C":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "C";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "c";
                }
                }

                break;
            case "V":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "V";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "v";
                }
                }

                break;
            case "B":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "B";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "b";
                }
                }

                break;
            case "N":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "N";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "n";
                }
                }

                break;
            case "M":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "M";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "m";
                }
                }

                break;
            case ",":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + ",";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + ",";
                }
                }

                break;
            case ".":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + ".";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + ".";
                }
                }

                break;
            case "-":
                if (!intOnly)
                {
                if (shiftActive)
                {
                    textField.text = textField.text + "_";
                    shiftActive = false;
                }
                else
                {
                    textField.text = textField.text + "-";
                }
                }

                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using UnityEngine.UI;
using Utils;
using System;

public class BrushMenu : MonoBehaviour
{

    #region Fields

    [SerializeField] private FalloffButton openFalloffsButton;
    [SerializeField] private GameObject menuFalloffSelect;

    [SerializeField] private Slider sliderSize;
    [SerializeField] private Text sliderSizeValue;

    [SerializeField] private Slider sliderHardness;
    [SerializeField] private Text sliderHardnessValue;

    [SerializeField] private Button acceptButton;

    private List<FalloffButton> falloffsButtons = new List<FalloffButton>();

    private Falloff[] falloffs;

    #endregion


    // Start is called before the first frame update
    void Start()
    {
        openFalloffsButton.GetComponent<Button>().onClick.AddListener(ToggleFalloffMenu);
        acceptButton.GetComponent<Button>().onClick.AddListener(AcceptOnClick);

        falloffs = Array.ConvertAll(StateManager.Instance.Notify(Message.GetFalloffs), item => item as Falloff);

        object[] brushData = StateManager.Instance.Notify(Message.GetBrush);
        string mID = System.Convert.ToString(brushData[0]);
        foreach (Falloff item in falloffs)
        {
            if (item.FalloffID.Equals(mID))
            {
                openFalloffsButton.GetComponent<RawImage>().texture = item.Image;
            }
        }
        
        sliderSize.value = System.Convert.ToInt32(brushData[1]);
        sliderHardness.value = System.Convert.ToSingle(brushData[2]);


        //Aktiven Brush Anzeigen
        //openFalloffsButton.GetComponent<Image>().sprite = activeFalloffImage.Image.sprite;

        foreach (Falloff item in falloffs)
        {
            FalloffButton falloffButton = ObjectUtils.Instantiate<FalloffButton>(menuFalloffSelect.transform, item.Name);
            falloffButton.transform.localScale = Vector3.one;
            falloffButton.FalloffID = item.FalloffID;
            falloffButton.GetComponent<RawImage>().texture = item.Image;
            falloffButton.GetComponent<Button>().onClick.AddListener(() => SelectFalloff(falloffButton));
            falloffsButtons.Add(falloffButton);
        }

        sliderSize.onValueChanged.AddListener((v) => NotifyOnChanged());
        sliderHardness.onValueChanged.AddListener((v) => NotifyOnChanged());

        sliderHardnessValue.text = sliderHardness.value.ToString();
        sliderSizeValue.text = sliderSize.value.ToString();
    }

    void ToggleFalloffMenu()
    {
        menuFalloffSelect.SetActive(!menuFalloffSelect.activeSelf);
    }

    void AcceptOnClick()
    {
        StateManager.Instance.CurrentProgramState = StateManager.Instance.LastProgramState;

        NotifyOnChanged();
        gameObject.SetActive(false);
    }

    void NotifyOnChanged()
    {
        int size = (int)sliderSize.value;
        float hardness = sliderHardness.value;
        string brushID = openFalloffsButton.FalloffID;

        StateManager.Instance.Notify(Message.SetBrush, brushID, size, hardness);

        sliderHardnessValue.text = sliderHardness.value.ToString();
        sliderSizeValue.text = sliderSize.value.ToString();
    }

    void SelectFalloff(FalloffButton falloffButton)
    {
        openFalloffsButton.GetComponent<RawImage>().texture = falloffButton.GetComponent<RawImage>().texture;
        openFalloffsButton.FalloffID = falloffButton.FalloffID;
        NotifyOnChanged();
        menuFalloffSelect.SetActive(!menuFalloffSelect.activeSelf);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using Utils;

[RequireComponent(typeof(TerrainManager))]
public class InteractiveTable : MonoBehaviour, IMessageReceiver
{
    #region Fields

    [SerializeField] private Table table;

    private TerrainManager tManager;

    #endregion

    #region Properties
    #endregion

    #region Functions

    // Start is called before the first frame update
    void Start()
    {
        StateManager.Instance.AddReceiver(this);

        tManager = GetComponent<TerrainManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Über Menü ändern
        if (ViveInput.GetPressDownEx(HandRole.RightHand, ControllerButton.Trigger)
        && StateManager.Instance.CurrentProgramState == StateManager.ProgramState.TableAdjustment
        && VivePose.IsValidEx(TrackerRole.Tracker1)
        && VivePose.IsValidEx(TrackerRole.Tracker2))
        {
            UpdateTransformation();
        }
    }

    private void ToggleTable()
    {
        table.gameObject.SetActive(!table.gameObject.activeSelf);
    }

    private void UpdateTransformation()
    {
        Debug.Log("Table adjusted.");

        Vector3 c1 = VivePose.GetPoseEx(TrackerRole.Tracker1).pos;
        Vector3 c3 = VivePose.GetPoseEx(TrackerRole.Tracker2).pos;

        Vector3 newCenter = (c1 + c3) * 0.5f;
        newCenter.y = 0;
        transform.position = newCenter;

        table.UpdateTable(2 * Mathf.Abs(newCenter.x - c1.x), 2 * Mathf.Abs(newCenter.z - c1.z), Mathf.Min(c1.y, c3.y));

        tManager.UpdateParentYPosition(Mathf.Min(c1.y, c3.y) + 0.2f);
    }

    public object[] ReceiveMessage(Message message, object[] data)
    {
        switch (message)
        {
            case Message.ToggleTable:
                ToggleTable();
                break;
        }
        return null;
    }



    #endregion
}

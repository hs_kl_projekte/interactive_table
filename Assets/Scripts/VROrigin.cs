﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using Utils;

public class VROrigin : MonoBehaviour
{
    #region Fields

    [SerializeField] private Transform mainCamera;
    [SerializeField] private Transform rightPointerUI;
    [SerializeField] private Transform leftPointerUI;

    private ModeMenu modeMenu;
    private BrushMenu brushMenu;
    private ControllerRadialMenu radialMenu;


    #endregion

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        modeMenu = ObjectUtils.Instantiate<ModeMenu>(null, "MainMenu");
        brushMenu = ObjectUtils.Instantiate<BrushMenu>(null, "BrushMenu");
        radialMenu = ObjectUtils.Instantiate<ControllerRadialMenu>(leftPointerUI, "RadialMenu");


        modeMenu.gameObject.SetActive(false);
        brushMenu.gameObject.SetActive(false);
        radialMenu.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (ViveInput.GetPressUpEx(HandRole.RightHand, ControllerButton.Menu))
        {
            if (StateManager.Instance.CurrentProgramState == StateManager.ProgramState.Menu)
            {
                StateManager.Instance.CurrentProgramState = StateManager.Instance.LastProgramState;
                ChangeMainMenuActive(false);
            }
            else
            {
                StateManager.Instance.LastProgramState = StateManager.Instance.CurrentProgramState;
                StateManager.Instance.CurrentProgramState = StateManager.ProgramState.Menu;

                DeactivateNonMainMenu();
                ChangeMainMenuActive(true);
            }
        }

        if (ViveInput.GetPressUpEx(HandRole.RightHand, ControllerButton.Pad)
        && ViveInput.ClickCountEx(HandRole.RightHand, ControllerButton.Pad) < 2
        && StateManager.Instance.CurrentProgramState == StateManager.ProgramState.Editing
        && StateManager.Instance.CurrentModelingState == StateManager.ModelingState.VertexManipulating)
        {
            ChangeBrushMenuActive(!brushMenu.gameObject.activeSelf);
        }


        if (ViveInput.GetPressUpEx(HandRole.LeftHand, ControllerButton.Pad)
        && StateManager.Instance.CurrentProgramState == StateManager.ProgramState.Editing)
        {
            if (ViveInput.ClickCount(HandRole.LeftHand, ControllerButton.Pad) == 2)
            {
                ChangeRadialManuActive(false);
                StateManager.Instance.CurrentModelingState = StateManager.ModelingState.None;
            }
            else
            {
                ChangeRadialManuActive(true);
            }
        }
    }

    private void ChangeMainMenuActive(bool active)
    {
        modeMenu.transform.position = mainCamera.position + mainCamera.forward;
        modeMenu.transform.LookAt(2 * modeMenu.transform.position - mainCamera.position, Vector3.up);

        modeMenu.gameObject.SetActive(active);
    }

    private void DeactivateNonMainMenu()
    {
        ChangeBrushMenuActive(false);
        ChangeRadialManuActive(false);
    }

    private void ChangeBrushMenuActive(bool active)
    {
        if (active)
        {
            StateManager.Instance.LastProgramState = StateManager.Instance.CurrentProgramState;
            StateManager.Instance.CurrentProgramState = StateManager.ProgramState.Menu;
        }

        brushMenu.transform.position = mainCamera.position + mainCamera.forward;
        brushMenu.transform.LookAt(2 * brushMenu.transform.position - mainCamera.position, Vector3.up);

        brushMenu.gameObject.SetActive(active);
    }

    private void ChangeRadialManuActive(bool active)
    {
        radialMenu.gameObject.SetActive(active);
    }
}

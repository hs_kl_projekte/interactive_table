public interface IMessageReceiver
{
    object[] ReceiveMessage(Message message, object[] data);
}
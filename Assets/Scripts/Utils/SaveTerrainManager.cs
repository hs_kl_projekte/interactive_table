﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class SaveTerrainManager
{
    private static string DATA_PATH = "/StreamingAssets/Terrain/";

    public static bool SaveTerrainData(SaveTerrainData dataToSave)
    {
        string filePath = Application.dataPath + DATA_PATH + dataToSave.name + ".json";

        string dataAsJson = JsonUtility.ToJson(dataToSave, true);
        FileInfo file = new FileInfo(filePath);

        try
        {
            file.Directory.Create();
            File.WriteAllText(file.FullName, dataAsJson);
        }
        catch (System.Exception)
        {

            Debug.LogWarning("Could not write to file at: " + filePath);
            return false;
        }

        Debug.Log("Terrain saved to " + file.Name);

        return true;
    }

    public static SaveTerrainData LoadTerrainData(string terrainName)
    {
        string filePath = Application.dataPath + DATA_PATH + terrainName + ".json";

        FileInfo file = new FileInfo(filePath);
        file.Directory.Create();


        if (file.Exists)
        {
            string dataAsJson = File.ReadAllText(file.FullName);
            Debug.Log("File " + file.Name + " loaded.");
            return JsonUtility.FromJson<SaveTerrainData>(dataAsJson);
        }
        else
        {
            Debug.LogWarning("SaveData not found at: " + filePath);
            return null;
        }
    }

    public static string[] GetSavedTerrains()
    {
        string filePath = Application.dataPath + DATA_PATH;

        DirectoryInfo dir = new DirectoryInfo(filePath);
        dir.Create();

        return dir.GetFiles().Where(f => !f.Name.Contains(".json.meta")).Select(s => s.Name.Replace(".json", "")).ToArray();
    }
}

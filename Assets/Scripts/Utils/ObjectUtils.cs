using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Utils
{
    public class ObjectUtils
    {

        public static T Instantiate<T>(Transform _parent = null, string _name = null)
        {
            string path = "Prefabs" + Path.AltDirectorySeparatorChar + typeof(T).Name;

            GameObject obj = Object.Instantiate(Resources.Load<GameObject>(path));
            T instance = obj.GetComponent<T>();

            if (obj != null)
            {
                //print("Gameobject found.");
                if (_parent != null)
                {
                    //print("Parent set.");
                    obj.transform.SetParent(_parent, false);
                }
                if (_name != null)
                {
                    obj.name = _name;
                }
            }
        
            return instance;
        }

        public static T[] InstantiateMultiple<T>(string _folderpath = null)
        {
            string path;
            if (_folderpath == null)
            {
                path = "Prefabs" + Path.AltDirectorySeparatorChar;
            }
            else
            {
                path = _folderpath;
            }
            List<T> instances = new List<T>();

            foreach (GameObject obj in Resources.LoadAll<GameObject>(path))
            {
                if (obj.GetComponent<T>() != null)
                {
                    instances.Add(Object.Instantiate(obj).GetComponent<T>());
                }
            }

            return instances.ToArray();
        }

    }
    //https://answers.unity.com/answers/979971/view.html
    public static class CoroutineTracker
    {
        private static List<IEnumerator> runningCoroutinesByEnumerator = new List<IEnumerator>();

        public static Coroutine StartTrackedCoroutine(this MonoBehaviour monobehaviour, IEnumerator _coroutine)
        {
            return monobehaviour.StartCoroutine(GenericRoutine(monobehaviour, _coroutine));
        }

        public static bool IsTrackedCoroutineRunning(this MonoBehaviour monobehaviour, IEnumerator _coroutine)
        {
            return runningCoroutinesByEnumerator.Contains(_coroutine);
        }

        public static void StopTrackedCoroutine(this MonoBehaviour monobehaviour, IEnumerator _coroutine)
        {
            if (!runningCoroutinesByEnumerator.Contains(_coroutine))
            {
                return;
            }

            monobehaviour.StopCoroutine(_coroutine);
            runningCoroutinesByEnumerator.Remove(_coroutine);
        }

        private static IEnumerator GenericRoutine(this MonoBehaviour monobehaviour, IEnumerator coroutine)
        {
            runningCoroutinesByEnumerator.Add(coroutine);
            yield return monobehaviour.StartCoroutine(coroutine);
            runningCoroutinesByEnumerator.Remove(coroutine);
        }

    }
}

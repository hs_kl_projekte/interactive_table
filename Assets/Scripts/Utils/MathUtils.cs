
using UnityEngine;

namespace Utils
{
    /// <summary>
    /// A collection of mathematical helper functions.
    /// </summary>
    public sealed class MathUtils
    {
        /// <summary>
        /// Maps a value from one number range to another.
        /// </summary>
        /// <param name="_oldMin">The input minimum value</param>
        /// <param name="_oldMax">The input maximum value</param>
        /// <param name="_newMin">The output minimum value</param>
        /// <param name="_newMax">The output maximum value</param>
        /// <param name="_value"></param>
        /// <returns></returns>
        public static float Map(float _oldMin, float _oldMax, float _newMin, float _newMax, float _value)
        {
            if (_value <= _oldMin)
            {
                return _newMin;
            }
            else if (_value >= _oldMax)
            {
                return _newMax;
            }

            return _newMin + (_value - _oldMin) / (_oldMax - _oldMin) * (_newMax - _newMin);
        }

        /// <summary>
        /// Calculates the average of an array of <see cref="Vector3"/>.
        /// </summary>
        /// <param name="_vectors"></param>
        /// <returns></returns>
        public static Vector3 MultiVectorAverage(params Vector3[] _vectors)
        {
            if (_vectors.Length == 0)
            {
                Debug.LogError("Array was empty!");
                return default(Vector3);
            }
            Vector3 outVector = _vectors[0];

            for (int i = 1; i < _vectors.Length; i++)
            {
                outVector = outVector + _vectors[i];
            }

            return outVector / _vectors.Length;
        }

        /// <summary>
        /// Calculates the average of two <see cref="Vector3"/>.
        /// </summary>
        /// <param name="_v1"></param>
        /// <param name="_v2"></param>
        /// <returns></returns>
        public static Vector3 VectorAverage(Vector3 _v1, Vector3 _v2)
        {
            return (_v1 + _v2) / 2;
        }

        /// <summary>
        /// Calculates the average of two <see cref="Vector2"/>.
        /// </summary>
        /// <param name="_v1"></param>
        /// <param name="_v2"></param>
        /// <returns></returns>
        public static Vector2 VectorAverage(Vector2 _v1, Vector2 _v2)
        {
            return (_v1 + _v2) / 2;
        }

        /// <summary>
        /// Rotates a <see cref="Vector3"/> some angle around a given pivot.
        /// </summary>
        /// <param name="_point"></param>
        /// <param name="_pivot"></param>
        /// <param name="_angles">Euler angle as <see cref="Vector3"/></param>
        /// <returns></returns>
        public static Vector3 RotateVectorAroundPivot(Vector3 _point, Vector3 _pivot, Vector3 _angles)
        {
            return RotateVectorAroundPivot(_point, _pivot, Quaternion.Euler(_angles));
        }

        /// <summary>
        /// Rotates a <see cref="Vector3"/> around a given pivot.
        /// </summary>
        /// <param name="_point"></param>
        /// <param name="_pivot"></param>
        /// <param name="_rotation"></param>
        /// <returns></returns>
        public static Vector3 RotateVectorAroundPivot(Vector3 _point, Vector3 _pivot, Quaternion _rotation)
        {
            return _rotation * (_point - _pivot) + _pivot;
        }


    }
}

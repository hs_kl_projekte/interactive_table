﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.XR;

public class RoomManager : MonoBehaviour
{
    #region Fields

    [SerializeField] private Transform[] positiveXDirection;
    [SerializeField] private Transform[] negativeXDirection;
    [SerializeField] private Transform[] positiveZDirection;
    [SerializeField] private Transform[] negativeZDirection;

    private List<Vector3> lighthousePosition;

    #endregion

    #region Functions

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        GetLighthousePosition();
    }

    private void GetLighthousePosition()
    {
        List<XRNodeState> nodeStates = new List<XRNodeState>();
        InputTracking.GetNodeStates(nodeStates);

        foreach (XRNodeState trackedNode in nodeStates.Where(n => n.nodeType == XRNode.TrackingReference))
        {
            bool hasPos = trackedNode.TryGetPosition(out var position);
            bool hasRot = trackedNode.TryGetRotation(out var rotation);
            if (hasPos)
            {
                Debug.Log("Sensor found at " + position);
                lighthousePosition.Add(position);
            }
        }
    }

    #endregion
}
